# OpenX CMS

A symfony 5x project.


## Features


## License


## Support


## Misc. Info

These are some dev notes for this project.

# Get The Latest Symfony LTS Version
We need this. It is NOT the full Symfony Framework, we ***NEED!*** this  for
this project. Why? Because it is eaier to make the *ROUTES* work. Using the FULL
 distribution, I spent 2 days and didn't get a single route to work. I spent
 about 10 miniutes of reading this <a href="" target="_blank">Article</a>.

 It's a great source and I recommend reading that.

```bash
symfony new my_project_name --version=lts
```
... *AWESOME!!!!* is's only 6.5 mb in size. he full was something like 38 mb, so
 this is great!. Now we only need to add what's REALLY NEEDED or (wanted).

----

## Symfony Development Server

I use this because this is inside the symfony cli and can be use with SSL (https://)

First go <a href="https://symfony.com/download" target="_blank">
<b>to Download Symfony</b></a> and find how to get it on Linux, MacOS, Windows.

***After that...***

### Enabling TLS

Browsing the secure version of your applications locally is important to detect problems with mixed content early, and to run libraries that only run in HTTPS. Traditionally this has been painful and complicated to set up, but the Symfony server automates everything. First, run this command:
```bash
 symfony server:ca:install
```

**Then Start The Server**
```bash
symfony server:start
```

*STRG+C* turns off the logging. After that stop the server

```bash
symfony server:stop
```
